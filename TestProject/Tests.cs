﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace TestProject
{
    [TestFixture(Platform.Android)]
    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void AppLaunches()
        {
            app.Screenshot("First screen.");

            // 1 - Repl
            //app.Repl();

            // 2 - Taps
            app.Tap("myButton");

            // 3 - Wait for
            app.WaitForElement(x => x.Marked("1 clicks!"));

            // 4 - Text input
            app.Tap("myEditText");
            app.EnterText("Det her er noget virkelig sjovt!");
            app.DismissKeyboard();
        }
    }
}
